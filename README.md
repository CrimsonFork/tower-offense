# Tower Offense

A local pvp Tower Defense game with Bullet Hell mechanics.

Playfield is a continous conveyor belt going from right to left with a stationary checkerboard pattern.

The Defender is on the right side, the Offender on the left.

## The Defender

The Defender can plant Turrets anywhere on his horizontal height, limited by available points, and customize each Turret's bulllet pattern. **type of customization yet to be determined**

The turrets then move at an equal, steady pace to the left side across the playfield, being activated ot one color of the checkerboard turrets, and deactivated at the other. **Which color? Different colors for different Turrets? […] types of Turrets?**

As the Turrets arrive at the left, they get destroyed. **some sort of bonus for destruction of towers at the left edge to the Offender to prevent both willfuil spam and pre-mature mass-destruction of towers?**

The Defender has a steadily filling bar of Ressources that limits his ability to build towers. Each time he gets damaged the Ressources get a boost. **speed or quantity or max.limit? Some mixture of all? Random selection?**

Ability to build walls that stop both the Offender and Defender's Tower's bullets.

## The Offender

The Offender's job is to guide his Drones through to the Defender's Base.

The Offender can only control one Drone at a time, but can attach further drones to follow the Main one. This malkes the Offender able to deal more damage at the Defender's base, but comes with the penalty that if one of the active Drones dies, all of them do. **Or maybe just start drifting furtheer in the same direction for potential hits?**

The Offender starts with one available Drone (Score of 1), which will be increadsed by one for every successful attempt and decreased by one for every unsuccessful attempt. **if the Offender gets an unsuccessful atempt but manages to still hit the Defender with a drifting turret, the Score stays unchanged (see above)**

## Win/Loose conditions



---

Bei Kontakt mit Angreifern explodieren die Türme mit Projektilen nach einem Countdown

Physische Interaktion?

